import type { Type } from '@/types/Type'
import http from './http'

type Product = {
  id?: number
  name: string
  price: number
  type: Type
  image: string
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products${product.image}`
}
export { type Product, getImageUrl }
